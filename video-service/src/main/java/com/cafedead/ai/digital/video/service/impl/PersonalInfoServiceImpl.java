/**
 * meituan.com Inc.
 * Copyright (c) 2010-2018 All Rights Reserved.
 */
package com.cafedead.ai.digital.video.service.impl;

import com.cafedead.ai.digital.video.common.vo.PersonalInfoVO;
import com.cafedead.ai.digital.video.dao.PersonalBaseInfoDAO;
import com.cafedead.ai.digital.video.dao.entity.PersonalBaseInfoDO;
import com.cafedead.ai.digital.video.service.PersonalInfoService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * <p>
 *
 * </p>
 * @author hujingshuang
 * @version $Id:PersonalInfoServiceImpl.java v1.0 2018/11/26 22:35 hujingshuang Exp $
 */
@Service
public class PersonalInfoServiceImpl implements PersonalInfoService {

    @Autowired
    PersonalBaseInfoDAO personalInfoDAO;

    public PersonalInfoVO queryPernalInfoByCardID(String cardID) {
        PersonalInfoVO personalInfoVO = new PersonalInfoVO();
        PersonalBaseInfoDO personalInfoDO = personalInfoDAO.queryPersonalBaseInfoByCardID(cardID);
        BeanUtils.copyProperties(personalInfoDO, personalInfoVO);
        return personalInfoVO;
    }

    public Boolean addPersonInfo(PersonalInfoVO personalInfoVO) {
        PersonalBaseInfoDO personalBaseInfoDO = new PersonalBaseInfoDO();
        BeanUtils.copyProperties(personalInfoVO, personalBaseInfoDO);
        return personalInfoDAO.addPersonInfo(personalBaseInfoDO);
    }
}
