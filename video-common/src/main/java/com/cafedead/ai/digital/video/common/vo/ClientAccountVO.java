/**
 * meituan.com Inc.
 * Copyright (c) 2010-2018 All Rights Reserved.
 */
package com.cafedead.ai.digital.video.common.vo;

import lombok.Data;

/**
 * <p>
 *
 * </p>
 * @author hujingshuang
 * @version $Id:ClientAccountVO.java v1.0 2018/12/2 21:42 hujingshuang Exp $
 */
@Data
public class ClientAccountVO {

    private String name;

    private String password;

    private String token;
}
