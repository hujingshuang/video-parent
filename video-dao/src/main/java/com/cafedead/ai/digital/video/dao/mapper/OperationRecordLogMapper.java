/**
 * meituan.com Inc.
 * Copyright (c) 2010-2018 All Rights Reserved.
 */
package com.cafedead.ai.digital.video.dao.mapper;

import com.cafedead.ai.digital.video.dao.entity.OperationRecordLogDO;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * <p>
 *
 * </p>
 * @author hujingshuang
 * @version $Id:OperationRecordLogMapper.java v1.0 2018/12/2 16:41 hujingshuang Exp $
 */
@Repository
public interface OperationRecordLogMapper {

    int insert(OperationRecordLogDO recordLogDO);

    List<OperationRecordLogDO> selectByKey(OperationRecordLogDO recordLogDO);
}
