/**
 * meituan.com Inc.
 * Copyright (c) 2010-2018 All Rights Reserved.
 */
package com.cafedead.ai.digital.video.dao.impl;

import com.cafedead.ai.digital.video.dao.ClientAccountDAO;
import com.cafedead.ai.digital.video.dao.entity.ClientAccountDO;
import com.cafedead.ai.digital.video.dao.mapper.ClientAccountMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * <p>
 *
 * </p>
 * @author hujingshuang
 * @version $Id:ClientAccountDAOImpl.java v1.0 2018/12/1 16:32 hujingshuang Exp $
 */
@Service
public class ClientAccountDAOImpl implements ClientAccountDAO {

    @Autowired
    ClientAccountMapper accountMapper;

    @Override
    public boolean isExistAccountName(String name) {
        return null != accountMapper.selectByName(name);
    }

    @Override
    public boolean isExistAccountToken(String token) {
        return null != accountMapper.selectByToken(token);
    }

    @Override
    public boolean addAccount(ClientAccountDO accountDO) {
        return 1 == accountMapper.insert(accountDO);
    }

    @Override
    public boolean deleteAccount(ClientAccountDO accountDO) {
        return 1 == accountMapper.deleteByToken(accountDO.getToken());
    }

    @Override
    public boolean updatePassword(ClientAccountDO accountDO) {
        return 1 == accountMapper.updateByName(accountDO);
    }

    @Override
    public ClientAccountDO queryAccount(String token) {
        return accountMapper.selectByToken(token);
    }

    @Override
    public ClientAccountDO queryAccountByName(String name) {
        return accountMapper.selectByName(name);
    }
}
