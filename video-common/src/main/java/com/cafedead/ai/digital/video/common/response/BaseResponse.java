/**
 * meituan.com Inc.
 * Copyright (c) 2010-2018 All Rights Reserved.
 */
package com.cafedead.ai.digital.video.common.response;

import com.cafedead.ai.digital.video.common.enums.ErrorEnum;
import lombok.Getter;
import lombok.Setter;

/**
 * <p>
 *
 * </p>
 * @author hujingshuang
 * @version $Id:BaseResponse.java v1.0 2018/12/1 15:00 hujingshuang Exp $
 */
@Setter
@Getter
public class BaseResponse<T> {

    private Boolean success;

    private String errorCode;

    private String errorMsg;

    private T data;

    public BaseResponse(T data) {
        if (data == null) {
            this.success = Boolean.FALSE;
            this.errorCode = ErrorEnum.SYSTEM_ERROR.getCode();
            this.errorMsg = ErrorEnum.SYSTEM_ERROR.getDesc();
        } else {
            this.success = Boolean.TRUE;
            this.data = data;
        }
    }

    public BaseResponse(ErrorEnum errorEnum) {
        this.success = Boolean.FALSE;
        this.errorCode = errorEnum.getCode();
        this.errorMsg = errorEnum.getDesc();
    }

}
