/**
 * meituan.com Inc.
 * Copyright (c) 2010-2018 All Rights Reserved.
 */
package com.cafedead.ai.digital.video.server.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 *
 * </p>
 * @author hujingshuang
 * @version $Id:MonitorCheckController.java v1.0 2018/11/26 11:58 hujingshuang Exp $
 */
@RestController
@RequestMapping(value = "/monitor")
public class MonitorCheckController {
    @RequestMapping(value = "/alive", method = RequestMethod.GET)
    public String alive() {
        return "ok";
    }
}
