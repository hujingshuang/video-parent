/**
 * meituan.com Inc.
 * Copyright (c) 2010-2018 All Rights Reserved.
 */
package com.cafedead.ai.digital.video.dao.mapper;

import com.cafedead.ai.digital.video.dao.entity.PersonalBaseInfoDO;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

/**
 * <p>
 *
 * </p>
 * @author hujingshuang
 * @version $Id:PersonalBaseInfoMapper.java v1.0 2018/11/26 21:46 hujingshuang Exp $
 */

@Repository
public interface PersonalBaseInfoMapper {

    PersonalBaseInfoDO queryPersonalBaseInfoByCardID(@Param("cardID") String cardID);

    int insert(PersonalBaseInfoDO personalBaseInfoDO);
}
