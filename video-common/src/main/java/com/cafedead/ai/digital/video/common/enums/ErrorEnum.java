/**
 * meituan.com Inc.
 * Copyright (c) 2010-2018 All Rights Reserved.
 */
package com.cafedead.ai.digital.video.common.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;
import org.springframework.util.StringUtils;

/**
 * <p>
 *
 * </p>
 * @author hujingshuang
 * @version $Id:ErrorEnum.java v1.0 2018/12/1 23:32 hujingshuang Exp $
 */

@Getter
@AllArgsConstructor
public enum ErrorEnum {

    SYSTEM_ERROR("00000", "系统异常"),
    AUTH_ERROR("00100", "用户无权限"),

    REGISTER_ACCOUNT_ERROR("00200", "注册账号失败"),
    UPDATE_PASSWORD_ERROR("00201", "修改密码失败"),
    DELETE_ACCOUNT_ERROR("00202", "注销账号失败"),

    ACCOUNT_NOT_EXIST("00203", "该账号不存在"),
    ACCOUNT_IS_INVALID("00204", "账号或密码无效"),
    ACCOUNT_NAME_EXIST("00205", "该用户名已被占用")


    ;

    private String code;
    private String desc;

}
