/**
 * meituan.com Inc.
 * Copyright (c) 2010-2018 All Rights Reserved.
 */
package com.cafedead.ai.digital.video.dao;

import com.cafedead.ai.digital.video.dao.entity.PersonalBaseInfoDO;

/**
 * <p>
 *
 * </p>
 * @author hujingshuang
 * @version $Id:PersonalBaseInfoDAO.java v1.0 2018/11/26 21:44 hujingshuang Exp $
 */
public interface PersonalBaseInfoDAO {
    PersonalBaseInfoDO queryPersonalBaseInfoByCardID(String carID);

    Boolean addPersonInfo(PersonalBaseInfoDO personalBaseInfoDO);
}
