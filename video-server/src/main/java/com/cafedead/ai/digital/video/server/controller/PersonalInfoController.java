/**
 * meituan.com Inc.
 * Copyright (c) 2010-2018 All Rights Reserved.
 */
package com.cafedead.ai.digital.video.server.controller;

import com.cafedead.ai.digital.video.common.vo.PersonalInfoVO;
import com.cafedead.ai.digital.video.service.PersonalInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * <p>
 *
 * </p>
 * @author hujingshuang
 * @version $Id:PersonalInfoController.java v1.0 2018/11/26 22:44 hujingshuang Exp $
 */

@RestController
@RequestMapping(value = "/personal")
public class PersonalInfoController {

    @Autowired
    PersonalInfoService personalInfoService;

    @GetMapping(value = "/getInfo")
    public PersonalInfoVO queryByCardID(@RequestParam(value = "cardId", required = true) String cardId) {
        return personalInfoService.queryPernalInfoByCardID(cardId);
    }

    @PostMapping(value = "/add")
    public Boolean add(@RequestBody PersonalInfoVO personalInfoVO) {
        return personalInfoService.addPersonInfo(personalInfoVO);
    }
}
