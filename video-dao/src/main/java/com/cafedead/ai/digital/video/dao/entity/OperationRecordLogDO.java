/**
 * meituan.com Inc.
 * Copyright (c) 2010-2018 All Rights Reserved.
 */
package com.cafedead.ai.digital.video.dao.entity;

import lombok.Data;

import java.util.Date;

/**
 * <p>
 *
 * </p>
 * @author hujingshuang
 * @version $Id:OperationRecordLogDO.java v1.0 2018/12/2 16:42 hujingshuang Exp $
 */
@Data
public class OperationRecordLogDO {

    private Long id;

    private String ip;

    private String name;

    private Byte event;

    private String record;

    private Date gmtCreate;

    private Date gmtModified;
}
