/**
 * meituan.com Inc.
 * Copyright (c) 2010-2018 All Rights Reserved.
 */
package com.cafedead.ai.digital.video.dao.impl;

import com.cafedead.ai.digital.video.dao.PersonalBaseInfoDAO;
import com.cafedead.ai.digital.video.dao.entity.PersonalBaseInfoDO;
import com.cafedead.ai.digital.video.dao.mapper.PersonalBaseInfoMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * <p>
 *
 * </p>
 * @author hujingshuang
 * @version $Id:PersonalBaseInfoDAOImpl.java v1.0 2018/11/26 21:45 hujingshuang Exp $
 */
@Service
public class PersonalBaseInfoDAOImpl implements PersonalBaseInfoDAO {

    @Autowired
    private PersonalBaseInfoMapper personalInfoMapper;

    @Override
    public PersonalBaseInfoDO queryPersonalBaseInfoByCardID(String carID) {
        return personalInfoMapper.queryPersonalBaseInfoByCardID(carID);
    }

    @Override
    public Boolean addPersonInfo(PersonalBaseInfoDO personalBaseInfoDO) {
        return 1 == personalInfoMapper.insert(personalBaseInfoDO);
    }
}
