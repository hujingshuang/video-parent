/**
 * meituan.com Inc.
 * Copyright (c) 2010-2018 All Rights Reserved.
 */
package com.cafedead.ai.digital.video.server.UtilTest;

import com.cafedead.ai.digital.video.server.util.TokenUtil;
import org.junit.Test;

/**
 * <p>
 *
 * </p>
 * @author hujingshuang
 * @version $Id:UtilTest.java v1.0 2018/12/2 00:50 hujingshuang Exp $
 */
public class UtilTest {

    private TokenUtil tokenUtil = new TokenUtil();

    @Test
    public void encryptionTest() {
        System.out.println(tokenUtil.getToken("刘德华", "MTIzNDU2Nw=="));
        System.out.println(tokenUtil.getToken("小明", "MTIzNDU2"));


        String name = "小明";
        // 123456:MTIzNDU2
        // 1234567:MTIzNDU2Nw==
        String password = "1234567";
        String pwd = tokenUtil.encryption(password);
        System.out.println(pwd);

        // 5bCP5piOJjEyMzQ1Ng==
        // 5bCP5piOJjEyMzQ1Njc=
        String token = tokenUtil.getToken(name, pwd);

        System.out.println(token);

    }
}
