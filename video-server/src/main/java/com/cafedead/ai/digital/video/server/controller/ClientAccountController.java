/**
 * meituan.com Inc.
 * Copyright (c) 2010-2018 All Rights Reserved.
 */
package com.cafedead.ai.digital.video.server.controller;

import com.alibaba.fastjson.JSON;
import com.cafedead.ai.digital.video.common.enums.ErrorEnum;
import com.cafedead.ai.digital.video.common.enums.OptEventEnum;
import com.cafedead.ai.digital.video.common.response.BaseResponse;
import com.cafedead.ai.digital.video.common.vo.ClientAccountVO;
import com.cafedead.ai.digital.video.common.vo.OperationRecordLogVO;
import com.cafedead.ai.digital.video.server.util.NetUtil;
import com.cafedead.ai.digital.video.server.util.TokenUtil;
import com.cafedead.ai.digital.video.service.ClientAccountService;
import com.cafedead.ai.digital.video.service.OperationRecordLogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

/**
 * <p>
 *
 * </p>
 * @author hujingshuang
 * @version $Id:ClientAccountController.java v1.0 2018/12/1 22:53 hujingshuang Exp $
 */
@SuppressWarnings("unchecked")
@RestController
@RequestMapping(value = "/account")
public class ClientAccountController {

    @Autowired
    ClientAccountService accountService;

    @Autowired
    TokenUtil tokenUtil;

    @Autowired
    OperationRecordLogService recordLogService;

    @Autowired
    NetUtil netUtil;

    @RequestMapping(value = "/register")
    public BaseResponse register(HttpServletRequest request) {
        try {
            ClientAccountVO accountVO = tokenUtil.getDecodeParam(request);

            if (StringUtils.isEmpty(accountVO.getName()) || StringUtils.isEmpty(accountVO.getPassword())) {
                return new BaseResponse(ErrorEnum.ACCOUNT_IS_INVALID);
            }

            // name 判重
            if (accountService.isExistAccountName(accountVO.getName())) {
                return new BaseResponse(ErrorEnum.ACCOUNT_NAME_EXIST);
            }

            boolean ret = accountService.addAccount(accountVO);
            if (ret) {
                OperationRecordLogVO recordLogVO = assembleRecordLogVO(request, OptEventEnum.ACCOUNT_REGISTER,
                    accountVO);
                recordLogService.addRecordLog(recordLogVO);

                return new BaseResponse(accountVO.getToken());
            }
            return new BaseResponse(ErrorEnum.REGISTER_ACCOUNT_ERROR);
        } catch (Exception e) {
            // TODO: 2018/12/2 打日志 
            return new BaseResponse(ErrorEnum.REGISTER_ACCOUNT_ERROR);
        }
    }

    @RequestMapping(value = "/updatePassword")
    public BaseResponse updatePassword(HttpServletRequest request) {
        try {
            ClientAccountVO newAccount = new ClientAccountVO();

            ClientAccountVO accountVO = tokenUtil.getDecodeParam(request);

            // 获取原账号信息
            ClientAccountVO originalAccount = accountService.queryAccountByToken(accountVO.getToken());
            if (originalAccount == null) {
                return new BaseResponse(ErrorEnum.ACCOUNT_NOT_EXIST);
            }

            // 生成新 token
            String token = tokenUtil.getToken(originalAccount.getName(), accountVO.getPassword());

            // 组装新账号信息
            newAccount.setName(originalAccount.getName());
            newAccount.setPassword(accountVO.getPassword());
            newAccount.setToken(token);

            boolean ret = accountService.updatePassword(newAccount);
            if (ret) {
                OperationRecordLogVO recordLogVO = assembleRecordLogVO(request, OptEventEnum.ACCOUNT_UPDATE_PASSWORD,
                    newAccount);
                recordLogService.addRecordLog(recordLogVO);

                return new BaseResponse(newAccount.getToken());
            }
            return new BaseResponse(ErrorEnum.UPDATE_PASSWORD_ERROR);
        } catch (Exception e) {
            // TODO: 2018/12/2 add log 
            return new BaseResponse(ErrorEnum.UPDATE_PASSWORD_ERROR);
        }
    }

    @RequestMapping(value = "/delete")
    public BaseResponse delete(HttpServletRequest request) {
        try {
            ClientAccountVO accountVO = tokenUtil.getDecodeParam(request);

            boolean ret = accountService.deleteAccount(accountVO);
            if (ret) {
                OperationRecordLogVO recordLogVO = assembleRecordLogVO(request, OptEventEnum.ACCOUNT_DESTROY,
                    accountVO);
                recordLogService.addRecordLog(recordLogVO);

                return new BaseResponse(true);
            }
            return new BaseResponse(ErrorEnum.DELETE_ACCOUNT_ERROR);
        } catch (Exception e) {
            // TODO: 2018/12/2 add log 
            return new BaseResponse(ErrorEnum.DELETE_ACCOUNT_ERROR);
        }
    }

    private OperationRecordLogVO assembleRecordLogVO(HttpServletRequest request, OptEventEnum event,
                                                     ClientAccountVO accountVO) {
        OperationRecordLogVO recordLogVO = new OperationRecordLogVO();
        recordLogVO.setIp(netUtil.getUserIp(request));
        recordLogVO.setName(accountVO.getName());
        recordLogVO.setEvent(event.getEvent());
        recordLogVO.setRecord(JSON.toJSONString(accountVO));
        return recordLogVO;
    }
}
