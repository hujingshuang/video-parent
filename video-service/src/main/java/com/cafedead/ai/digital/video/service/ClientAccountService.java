/**
 * meituan.com Inc.
 * Copyright (c) 2010-2018 All Rights Reserved.
 */
package com.cafedead.ai.digital.video.service;

import com.cafedead.ai.digital.video.common.vo.ClientAccountVO;

/**
 * <p>
 *
 * </p>
 * @author hujingshuang
 * @version $Id:ClientAccountService.java v1.0 2018/12/1 16:12 hujingshuang Exp $
 */
public interface ClientAccountService {

    boolean isExistAccountName(String name);

    boolean isExistAccountToken(String token);

    boolean addAccount(ClientAccountVO accountParam);

    boolean updatePassword(ClientAccountVO accountParam);

    boolean deleteAccount(ClientAccountVO accountParam);

    ClientAccountVO queryAccountByToken(String token);

    ClientAccountVO queryAccountByToken(String token, Class<?> clazz);
}
