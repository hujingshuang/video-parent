/**
 * meituan.com Inc.
 * Copyright (c) 2010-2018 All Rights Reserved.
 */
package com.cafedead.ai.digital.video.common.vo;

import lombok.Data;

import java.util.Date;

/**
 * <p>
 *
 * </p>
 * @author hujingshuang
 * @version $Id:PersonalInfoVO.java v1.0 2018/11/26 22:39 hujingshuang Exp $
 */
@Data
public class PersonalInfoVO {

    private Long id;

    /**
     * 姓名
     */
    private String name;

    /**
     * 年龄
     */
    private Byte age;

    /**
     * 性别（0 - 男，1 - 女）
     */
    private Byte sex;

    /**
     * 出生年月日（yyyy-MM-dd）
     */
    private String birthday;

    /**
     * 身份证号
     */
    private String cardID;

    /**
     * 图片
     */
    private String picture;

    /**
     * 国家
     */
    private String country;

    /**
     * 省份
     */
    private String province;

    /**
     * 城市
     */
    private String city;

    /**
     * 区/县
     */
    private String county;

    /**
     * 地址
     */
    private String address;

    /**
     * 电话
     */
    private String phone;

    /**
     * 数据状态（0 - 正常，1 - 删除）
     */
    private Byte status;

    private Date gmtCreate;

    private Date gmtModified;
}
