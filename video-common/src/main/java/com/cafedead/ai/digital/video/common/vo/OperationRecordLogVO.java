/**
 * meituan.com Inc.
 * Copyright (c) 2010-2018 All Rights Reserved.
 */
package com.cafedead.ai.digital.video.common.vo;

import lombok.Data;

/**
 * <p>
 *
 * </p>
 * @author hujingshuang
 * @version $Id:OperationRecordLogVO.java v1.0 2018/12/2 17:19 hujingshuang Exp $
 */
@Data
public class OperationRecordLogVO {
    private String ip;
    private String name;
    private Byte event;
    private String record;
    private String gmtCreate;
    private String gmtModified;
}
