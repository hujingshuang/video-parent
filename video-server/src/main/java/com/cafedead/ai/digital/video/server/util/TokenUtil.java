/**
 * meituan.com Inc.
 * Copyright (c) 2010-2018 All Rights Reserved.
 */
package com.cafedead.ai.digital.video.server.util;

import com.cafedead.ai.digital.video.common.request.BaseRequest;
import com.cafedead.ai.digital.video.common.vo.ClientAccountVO;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import javax.servlet.http.HttpServletRequest;
import java.util.Base64;

/**
 * <p>
 *
 * </p>
 * @author hujingshuang
 * @version $Id:TokenUtil.java v1.0 2018/12/1 14:56 hujingshuang Exp $
 */
@Component
public class TokenUtil {

    private final Base64.Decoder decoder = Base64.getDecoder();
    private final Base64.Encoder encoder = Base64.getEncoder();

    private static final String CONNECTOR = "&";
    /**
     * 解密 HttpServletRequest
     * @param request
     * @return
     */
    public ClientAccountVO getDecodeParam(HttpServletRequest request) {
        String name = request.getParameter("name");
        String password = request.getParameter("password");
        String token = request.getParameter("token");

        return decodeParam(name, password, token);
    }

    /**
     * 解密 BaseRequest
     * @param request
     * @return
     */
    public ClientAccountVO getDecodeParam(BaseRequest request) {
        String name = request.getName();
        String password = request.getPassword();
        String token = request.getToken();

        return decodeParam(name, password, token);
    }


    public String getToken(String name, String password) {
        password = decryption(password);
        return encryption(assemble(name, password));
    }

    /**
     * 若 token 不存在，则通过 name, password 生成回填
     * @param name
     * @param password
     * @return
     */
    private ClientAccountVO decodeParam(String name, String password, String token) {
        ClientAccountVO accountVO = new ClientAccountVO();
        accountVO.setName(name);
        accountVO.setPassword(password);
        if (StringUtils.isEmpty(token)) {
            password = decryption(password);
            token = encryption(assemble(name, password));
        }
        accountVO.setToken(token);
        return accountVO;
    }

    private String assemble(String name, String password) {
        return name + CONNECTOR + password;
    }

    /**
     * 加密
     * @param value
     * @return
     */
    public String encryption(String value) {
        if (StringUtils.isEmpty(value)) {
            return null;
        }
        return encoder.encodeToString(value.getBytes());
    }

    /**
     * 解密
     * @param value
     * @return
     */
    public String decryption(String value) {
        if (StringUtils.isEmpty(value)) {
            return null;
        }
        return new String(decoder.decode(value));
    }
}
