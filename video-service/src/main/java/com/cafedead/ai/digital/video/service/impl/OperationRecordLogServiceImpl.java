/**
 * meituan.com Inc.
 * Copyright (c) 2010-2018 All Rights Reserved.
 */
package com.cafedead.ai.digital.video.service.impl;

import com.cafedead.ai.digital.video.common.vo.OperationRecordLogVO;
import com.cafedead.ai.digital.video.dao.OperationRecordLogDAO;
import com.cafedead.ai.digital.video.dao.entity.OperationRecordLogDO;
import com.cafedead.ai.digital.video.service.OperationRecordLogService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.List;
import java.util.stream.Collectors;

/**
 * <p>
 *
 * </p>
 * @author hujingshuang
 * @version $Id:OperationRecordLogServiceImpl.java v1.0 2018/12/2 17:17 hujingshuang Exp $
 */
@Service
public class OperationRecordLogServiceImpl implements OperationRecordLogService {

    @Autowired
    private OperationRecordLogDAO recordLogDAO;

    @Override
    public boolean addRecordLog(OperationRecordLogVO recordLogVO) {
        OperationRecordLogDO recordLogDO = new OperationRecordLogDO();
        BeanUtils.copyProperties(recordLogVO, recordLogDO);
        return recordLogDAO.addOptRecordLog(recordLogDO);
    }

    @Override
    public List<OperationRecordLogVO> queryByKey(OperationRecordLogVO recordLogVO) {
        OperationRecordLogDO recordLogDO = new OperationRecordLogDO();
        BeanUtils.copyProperties(recordLogVO, recordLogDO);

        List<OperationRecordLogDO> recordLogDOList = recordLogDAO.queryByKey(recordLogDO);
        if (CollectionUtils.isEmpty(recordLogDOList)) {
            return null;
        }

        return recordLogDOList.stream().map(recordDO -> {
            OperationRecordLogVO recordVO = new OperationRecordLogVO();
            BeanUtils.copyProperties(recordDO, recordVO);
            return recordVO;
        }).collect(Collectors.toList());
    }
}
