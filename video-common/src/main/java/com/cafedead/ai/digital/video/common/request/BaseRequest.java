/**
 * meituan.com Inc.
 * Copyright (c) 2010-2018 All Rights Reserved.
 */
package com.cafedead.ai.digital.video.common.request;

import lombok.Getter;
import lombok.Setter;

/**
 * <p>
 *
 * </p>
 * @author hujingshuang
 * @version $Id:BaseRequest.java v1.0 2018/12/1 14:57 hujingshuang Exp $
 */

@Getter
@Setter
public class BaseRequest {

    private String name;

    private String password;

    private String token;
}
