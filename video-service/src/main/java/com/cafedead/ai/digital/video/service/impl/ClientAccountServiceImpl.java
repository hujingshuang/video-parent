/**
 * meituan.com Inc.
 * Copyright (c) 2010-2018 All Rights Reserved.
 */
package com.cafedead.ai.digital.video.service.impl;

import com.cafedead.ai.digital.video.common.vo.ClientAccountVO;
import com.cafedead.ai.digital.video.dao.ClientAccountDAO;
import com.cafedead.ai.digital.video.dao.entity.ClientAccountDO;
import com.cafedead.ai.digital.video.service.ClientAccountService;
import com.cafedead.ai.digital.video.service.cache.ClientAccountCache;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * <p>
 *
 * </p>
 * @author hujingshuang
 * @version $Id:ClientInfoServiceImpl.java v1.0 2018/12/1 16:21 hujingshuang Exp $
 */
@Service
public class ClientAccountServiceImpl implements ClientAccountService {

    @Autowired
    ClientAccountDAO accountDAO;


    @Override
    public boolean isExistAccountName(String name) {
        return accountDAO.isExistAccountName(name);
    }

    @Override
    public boolean isExistAccountToken(String token) {
        ClientAccountVO accountVO = ClientAccountCache.getAccount(token);
        return (accountVO != null);
    }

    @Override
    public boolean addAccount(ClientAccountVO accountVO) {
        ClientAccountDO accountDO = new ClientAccountDO();
        BeanUtils.copyProperties(accountVO, accountDO);
        boolean ret = accountDAO.addAccount(accountDO);
        if (ret) {
            ClientAccountCache.putAccount(accountVO.getToken(), accountVO);
        }
        return ret;
    }

    @Override
    public boolean deleteAccount(ClientAccountVO accountVO) {
        ClientAccountDO accountDO = new ClientAccountDO();
        BeanUtils.copyProperties(accountVO, accountDO);

        boolean ret = accountDAO.deleteAccount(accountDO);
        if (ret) {
            ClientAccountCache.clearAccount(accountVO.getToken());
        }

        return ret;
    }

    @Override
    public boolean updatePassword(ClientAccountVO accountVO) {
        ClientAccountDO accountDO = new ClientAccountDO();
        BeanUtils.copyProperties(accountVO, accountDO);
        boolean ret = accountDAO.updatePassword(accountDO);
        if (ret) {
            ClientAccountCache.putAccount(accountVO.getToken(), accountVO);
        }
        return ret;
    }

    @Override
    public ClientAccountVO queryAccountByToken(String token) {
        return ClientAccountCache.getAccount(token);
    }

    @Override
    public ClientAccountVO queryAccountByToken(String token, Class<?> clazz) {
        ClientAccountDO accountDO = accountDAO.queryAccount(token);
        if (accountDO == null) {
            return null;
        }
        ClientAccountVO accountVO = new ClientAccountVO();
        BeanUtils.copyProperties(accountDO, accountVO);
        return accountVO;
    }
}
