/**
 * meituan.com Inc.
 * Copyright (c) 2010-2018 All Rights Reserved.
 */
package com.cafedead.ai.digital.video.dao;

import com.cafedead.ai.digital.video.dao.entity.ClientAccountDO;

/**
 * <p>
 *
 * </p>
 * @author hujingshuang
 * @version $Id:ClientAccountDAO.java v1.0 2018/12/1 16:23 hujingshuang Exp $
 */
public interface ClientAccountDAO {
    boolean isExistAccountName(String name);

    boolean isExistAccountToken(String token);

    boolean addAccount(ClientAccountDO accountDO);

    boolean updatePassword(ClientAccountDO accountDO);

    boolean deleteAccount(ClientAccountDO accountDO);

    ClientAccountDO queryAccount(String token);

    ClientAccountDO queryAccountByName(String name);
}
