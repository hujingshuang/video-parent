/**
 * meituan.com Inc.
 * Copyright (c) 2010-2018 All Rights Reserved.
 */
package com.cafedead.ai.digital.video.service.cache;

import com.cafedead.ai.digital.video.common.vo.ClientAccountVO;
import com.cafedead.ai.digital.video.service.ClientAccountService;
import com.cafedead.ai.digital.video.service.util.SpringContextUtil;
import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;

/**
 * <p>
 *
 * </p>
 * @author hujingshuang
 * @version $Id:ClientAccountCache.java v1.0 2018/12/2 20:58 hujingshuang Exp $
 */
public class ClientAccountCache {

    private static LoadingCache<String, ClientAccountVO> cache = CacheBuilder.newBuilder()
        .build(new CacheLoader<String, ClientAccountVO>() {

            private ClientAccountService accountService = SpringContextUtil
                .getBean("clientAccountServiceImpl", ClientAccountService.class);

            /**
             * 缓存未命中时，调用load方法获取
             * @param token
             * @return
             */
            @Override
            public ClientAccountVO load(String token) {
                return getAccountByName(token);
            }

            private ClientAccountVO getAccountByName(String token) {
                try {
                    ClientAccountVO accountVO = accountService.queryAccountByToken(token, ClientAccountVO.class);
                    if (accountVO != null) {
                        putAccount(accountVO.getToken(), accountVO);
                    }
                    return accountVO;
                } catch (Exception e) {
                    // TODO: 2018/12/2 add log 
                    return null;
                }
            }
        });

    /**
     * 按 key 获取
     * @param token
     * @return
     */
    public static ClientAccountVO getAccount(String token) {
        try {
            return cache.get(token);
        } catch (Exception e) {
            // TODO: 2018/12/2 add log
            return null;
        }
    }

    public static void putAccount(String token, ClientAccountVO accountVO) {
        try {
            cache.put(token, accountVO);
        } catch (Exception e) {
            // TODO: 2018/12/2 add log
        }
    }

    /**
     * 按 key 清除
     * @param token
     */
    public static void clearAccount(String token) {
        try {
            cache.invalidate(token);
        } catch (Exception e) {
            // TODO: 2018/12/2 add log
        }
    }

    /**
     * 按 key 刷新
     * @param token
     */
    public static void refreshAccount(String token) {
        try {
            cache.refresh(token);
        } catch (Exception e) {
            // TODO: 2018/12/2 add log
        }
    }

}
