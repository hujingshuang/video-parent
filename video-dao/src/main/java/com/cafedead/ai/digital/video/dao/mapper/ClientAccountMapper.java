/**
 * meituan.com Inc.
 * Copyright (c) 2010-2018 All Rights Reserved.
 */
package com.cafedead.ai.digital.video.dao.mapper;

import com.cafedead.ai.digital.video.dao.entity.ClientAccountDO;
import org.springframework.stereotype.Repository;

/**
 * <p>
 *
 * </p>
 * @author hujingshuang
 * @version $Id:ClientManageMapper.java v1.0 2018/12/1 16:34 hujingshuang Exp $
 */

@Repository
public interface ClientAccountMapper {
    ClientAccountDO selectByName(String name);

    ClientAccountDO selectByToken(String token);

    int updateByName(ClientAccountDO accountDO);

    int insert(ClientAccountDO accountDO);

    int deleteByName(String token);

    int deleteByToken(String token);
}
