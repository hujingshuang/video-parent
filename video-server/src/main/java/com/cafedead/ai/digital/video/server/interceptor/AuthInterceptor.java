/**
 * cafedead.cn Inc.
 * Copyright（c）2016-2018 All Rights Reserved.
 */
package com.cafedead.ai.digital.video.server.interceptor;

import com.cafedead.ai.digital.video.common.vo.ClientAccountVO;
import com.cafedead.ai.digital.video.server.util.TokenUtil;
import com.cafedead.ai.digital.video.service.ClientAccountService;
import com.cafedead.ai.digital.video.service.cache.ClientAccountCache;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * <p>
 *
 * </p>
 * @author HJS
 * @version $Id:AuthInterceptor.java v1.0 2018/11/30 0:24 HJS Exp $
 */
public class AuthInterceptor implements HandlerInterceptor {

    @Autowired
    TokenUtil tokenUtil;

    @Autowired
    ClientAccountService accountService;


    @Override
    public boolean preHandle(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Object o) {
        ClientAccountVO accountVO = tokenUtil.getDecodeParam(httpServletRequest);
        if (StringUtils.isEmpty(accountVO.getToken())) {
            return false;
        }
        return accountService.isExistAccountToken(accountVO.getToken());
    }

    @Override
    public void postHandle(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Object o,
                           ModelAndView modelAndView) {

    }

    @Override
    public void afterCompletion(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse,
                                Object o, Exception e) {

    }
}
