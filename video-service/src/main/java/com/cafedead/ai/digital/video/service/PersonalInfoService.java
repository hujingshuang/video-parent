/**
 * meituan.com Inc.
 * Copyright (c) 2010-2018 All Rights Reserved.
 */
package com.cafedead.ai.digital.video.service;

import com.cafedead.ai.digital.video.common.vo.PersonalInfoVO;

/**
 * <p>
 *
 * </p>
 * @author hujingshuang
 * @version $Id:PersonalInfoService.java v1.0 2018/11/26 22:35 hujingshuang Exp $
 */
public interface PersonalInfoService {

    PersonalInfoVO queryPernalInfoByCardID(String cardID);

    Boolean addPersonInfo(PersonalInfoVO personalInfoVO);
}
