/**
 * meituan.com Inc.
 * Copyright (c) 2010-2018 All Rights Reserved.
 */
package com.cafedead.ai.digital.video.dao;

import com.cafedead.ai.digital.video.dao.entity.OperationRecordLogDO;

import java.util.List;

/**
 * <p>
 *
 * </p>
 * @author hujingshuang
 * @version $Id:OperationRecordLogDAO.java v1.0 2018/12/2 17:11 hujingshuang Exp $
 */
public interface OperationRecordLogDAO {

    boolean addOptRecordLog(OperationRecordLogDO recordLogDO);

    List<OperationRecordLogDO> queryByKey(OperationRecordLogDO recordLogDO);
}
