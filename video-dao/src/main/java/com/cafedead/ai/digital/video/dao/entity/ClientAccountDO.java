/**
 * meituan.com Inc.
 * Copyright (c) 2010-2018 All Rights Reserved.
 */
package com.cafedead.ai.digital.video.dao.entity;

import lombok.Data;

import java.util.Date;

/**
 * <p>
 *
 * </p>
 * @author hujingshuang
 * @version $Id:ClientAccountDO.java v1.0 2018/12/1 16:25 hujingshuang Exp $
 */
@Data
public class ClientAccountDO {

    private Long id;

    private String name;

    private String password;

    private String token;

    private Date gmtCreate;

    private Date gmtModified;
}
