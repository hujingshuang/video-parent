CREATE TABLE `personal_base_info` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '自增id',
  `name` varchar(20) NOT NULL COMMENT '姓名',
  `age` tinyint(4) NOT NULL COMMENT '年龄',
  `sex` tinyint(4) NOT NULL COMMENT '性别（0 - 男，1 - 女）',
  `birthday` varchar(20) DEFAULT NULL COMMENT '出生年月（yyyy-MM-dd）',
  `card_id` varchar(50) NOT NULL COMMENT '身份证号',
  `picture` varchar(120) DEFAULT NULL COMMENT '照片',
  `country` varchar(50) NOT NULL COMMENT '国家',
  `province` varchar(50) NOT NULL COMMENT '省份',
  `city` varchar(50) NOT NULL COMMENT '城市',
  `county` varchar(50) NOT NULL COMMENT '区/县',
  `address` varchar(120) NOT NULL COMMENT '家庭地址',
  `phone` varchar(50) NOT NULL COMMENT '手机号',
  `status` tinyint(4) NOT NULL DEFAULT 0 COMMENT '数据状态（0 - 正常，1 - 删除）',
  `gmt_create` datetime DEFAULT CURRENT_TIMESTAMP,
  `gmt_modified` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uk_idx_card_id` (`card_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='个人基本信息'

CREATE TABLE `client_account_info` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '自增id',
  `name` varchar(64) NOT NULL COMMENT '账号',
  `password` varchar (64) NOT NULL COMMENT '密码',
  `token` varchar(128) NOT NULL COMMENT 'token',
  `gmt_create` datetime DEFAULT CURRENT_TIMESTAMP,
  `gmt_modified` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uk_idx_name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='客户端账号信息'

CREATE TABLE `operation_record_log` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '自增id',
  `ip` varchar(200) NOT NULL COMMENT '用户ip',
  `name` varchar (64) NOT NULL COMMENT '用户名',
  `event` tinyint(4) NOT NULL COMMENT '事件',
  `record` varchar(200) NOT NULL COMMENT '记录',
  `gmt_create` datetime DEFAULT CURRENT_TIMESTAMP,
  `gmt_modified` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='客户端用户操作记录日志'


