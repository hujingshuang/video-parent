/**
 * meituan.com Inc.
 * Copyright (c) 2010-2018 All Rights Reserved.
 */
package com.cafedead.ai.digital.video.dao.impl;

import com.cafedead.ai.digital.video.dao.OperationRecordLogDAO;
import com.cafedead.ai.digital.video.dao.entity.OperationRecordLogDO;
import com.cafedead.ai.digital.video.dao.mapper.OperationRecordLogMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 *
 * </p>
 * @author hujingshuang
 * @version $Id:OperationRecordLogDAOImpl.java v1.0 2018/12/2 17:13 hujingshuang Exp $
 */
@Service
public class OperationRecordLogDAOImpl implements OperationRecordLogDAO {

    @Autowired
    OperationRecordLogMapper recordLogMapper;

    @Override
    public boolean addOptRecordLog(OperationRecordLogDO recordLogDO) {
        return 1 == recordLogMapper.insert(recordLogDO);
    }

    @Override
    public List<OperationRecordLogDO> queryByKey(OperationRecordLogDO recordLogDO) {
        return recordLogMapper.selectByKey(recordLogDO);
    }
}
