/**
 * meituan.com Inc.
 * Copyright (c) 2010-2018 All Rights Reserved.
 */
package com.cafedead.ai.digital.video.service;

import com.cafedead.ai.digital.video.common.vo.OperationRecordLogVO;

import java.util.List;

/**
 * <p>
 *
 * </p>
 * @author hujingshuang
 * @version $Id:OperationRecordLogService.java v1.0 2018/12/2 17:17 hujingshuang Exp $
 */
public interface OperationRecordLogService {
    boolean addRecordLog(OperationRecordLogVO recordLogVO);

    List<OperationRecordLogVO> queryByKey(OperationRecordLogVO recordLogVO);
}
