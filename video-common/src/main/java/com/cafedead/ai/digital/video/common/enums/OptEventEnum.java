/**
 * meituan.com Inc.
 * Copyright (c) 2010-2018 All Rights Reserved.
 */
package com.cafedead.ai.digital.video.common.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * <p>
 *
 * </p>
 * @author hujingshuang
 * @version $Id:OptEventEnum.java v1.0 2018/12/2 12:41 hujingshuang Exp $
 */
@Getter
@AllArgsConstructor
public enum OptEventEnum {

    LOG_IN((byte)0, "登录"),
    LOG_OUT((byte)1, "退出"),
    ACCOUNT_REGISTER((byte)2, "注册账号"),
    ACCOUNT_UPDATE_PASSWORD((byte)3, "修改密码"),
    ACCOUNT_DESTROY((byte)4, "注销账号")
    ;

    private byte event;
    private String desc;

}
